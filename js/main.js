//Responsive Navigation
$(document).ready(function() {
  $('body').addClass('js');
  var $menu = $('.site-nav-container'),
  $menulink = $('.menu-link'),
  $menuTrigger = $('.menu-item-has-children > a'),
  $searchLink = $('.search-link'),
  $siteSearch = $('.search-module'),
  $siteWrap = $('.site-wrap');

  $searchLink.click(function(e) {
    e.preventDefault();
    $searchLink.toggleClass('active');
    $siteSearch.toggleClass('active');
    $('#search-site').focus();
  });

  $menulink.click(function(e) {
    e.preventDefault();
    $menulink.toggleClass('active');
    $menu.toggleClass('active');
    $siteWrap.toggleClass('nav-active');
  });

  $('.menu-item-has-children').append("<span class='m-subnav-arrow'></span>");

  $('.site-nav .m-subnav-arrow').click(function() {
    $(this).toggleClass('active');
    $(this).parent('.menu-item-has-children').toggleClass('active');
    var $this = $(this).prev(".sn-level-2");
    $this.toggleClass('active').next('ul').toggleClass('active');
  });
  $('.side-nav .m-subnav-arrow').click(function() {
    $(this).toggleClass('active');
    $(this).parent('.menu-item-has-children').toggleClass('active');
    $(this).parent('.menu-item-has-children').children('ul').toggleClass('active');
  });

});
//Magnific Popup
$(document).ready(function() {
  $('.lightbox').magnificPopup({
    type: 'image',
    removalDelay: 500, //Delaying the removal in order to fit in the animation of the popup
    mainClass: 'mfp-fade', //The actual animation
  });
});
$(document).ready(function() {
  $('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
    disableOn: 700,
    type: 'iframe',
    mainClass: 'mfp-fade',
    removalDelay: 500,
    preloader: false,

    fixedContentPos: false,
    iframe: {
      patterns: {
        youtube: {
          index: 'youtube.com/', // String that detects type of video (in this case YouTube). Simply via url.indexOf(index).
          id: 'v=', // String that splits URL in a two parts, second part should be %id%
          // Or null - full URL will be returned
          // Or a function that should return %id%, for example:
          // id: function(url) { return 'parsed id'; }

          src: '//www.youtube.com/embed/%id%?autoplay=1&rel=0' // URL that will be set as a source for iframe.
        }
      },
      srcAction: 'iframe_src', // Templating object key. First part defines CSS selector, second attribute. "iframe_src" means: find "iframe" and set attribute "src".
    }
  });
});




//Delayed Popup with localstorage to show popup only once
$(document).ready(function() {
  var findPopupId = $('#delayed-popup').length; // if #delayed-popup exists, findPopupId = 1;
  if (findPopupId > 0) { // only run when #delayed-popup exists
    var wWidth = $(window).width(); // set variable of window width
    if (wWidth >= 640) { //only trigger on tablet or larger to prevent mobile private browsers who don't allow cookies (safari)
      if (localStorage.getItem('popup_show') === null && localStorage.getItem('exitintent_show') === null ) { // check if key is present in local storage to prevent re-triggering
        setTimeout(function() {
          window.$.magnificPopup.open({
            items: {
              src: '#delayed-popup' //ID of inline element
            },
            type: 'inline',
            removalDelay: 500, // delaying the removal in order to fit in the animation of the popup
            mainClass: 'mfp-fade mfp-fade-side', // The actual animation
          });
          localStorage.setItem('popup_show', 'true'); // set the key in local storage
        }, 11000); // delay in millliseconds until the modal triggers
      }
    }
  }
});




// Exit-Intent Modal
$(document).ready(function() {
  // Exit intent
  function addEvent(obj, evt, fn) {
    if (obj.addEventListener) {
      obj.addEventListener(evt, fn, false);
    } else if (obj.attachEvent) {
      obj.attachEvent("on" + evt, fn);
    }
  }
  // Exit intent trigger
  var findExitId = $('#exit-popup').length; // if #exit-popup exists, findExitId will contain a value of 1 (or more);
    if(findExitId > 0){ // if findExitId is greater than 0, it means that element exits on the page, therefore execute this code;
    addEvent(document, 'mouseout', function(evt) {
      if (evt.toElement === null && evt.relatedTarget === null && !localStorage.getItem('exitintent_show')) {
      //alert('test');
        window.$.magnificPopup.open({
          items: {
            src: '#exit-popup' //ID of inline element
          },
          type: 'inline',
          removalDelay: 500, //Delaying the removal in order to fit in the animation of the popup
          mainClass: 'mfp-fade mfp-fade-side', //The actual animation
        });
        localStorage.setItem('exitintent_show', 'true'); // Set the flag in localStorage
      }
    });
  }
});


//Show More
$(document).ready(function() {
  $(".showmore").after("<p><a href='#' class='show-more-link'>More</a></p>");
  var $showmorelink = $('.showmore-link');
  $showmorelink.click(function() {
    var $this = $(this);
    var $showmorecontent = $('.showmore');
    $this.toggleClass('active');
    $showmorecontent.toggleClass('active');
    return false;
  });
});

//Click to Expand
$(document).ready(function() {
  var $expandlink = $('.ce-header');
  $expandlink.click(function() {
    var $this = $(this);
    var $showmorecontent = $('.showmore');


    $this.parent().toggleClass('active'); 
    $showmorecontent.toggleClass('active');
    return false;
  });
});


// Accordion Tabs
$(document).ready(function () {
  $('.accordion-tabs').each(function(index) {
    $(this).children('li').first().children('a').addClass('is-active').next().addClass('is-open').show();
  });
  $('.accordion-tabs').on('click', 'li > a.tab-link', function(event) {
    if (!$(this).hasClass('is-active')) {
      event.preventDefault();
      var accordionTabs = $(this).closest('.accordion-tabs');
      accordionTabs.find('.is-open').removeClass('is-open').hide();

      $(this).next().toggleClass('is-open').toggle();
      accordionTabs.find('.is-active').removeClass('is-active');
      $(this).addClass('is-active');
    } else {
      event.preventDefault();
    }
  });
});


//Flexslider    
$(window).load(function() {
   
  $('.home-slider').flexslider({
    animation: "slide",
    animationLoop: true,
    controlNav: true,
    directionNav: false,
    slideshow: true,
   });

  $(".tbm-carousel").owlCarousel({
    loop:true, 
    responsiveClass: true,
    autoplayHoverPause:true,
    autoplay:true,
    nav: true,
    autoplayTimeout: 6000,
    responsive:{
      0:{
        margin:20,
        items:2
      },
      640:{
        margin:35,
        items:3
      },
      960:{
        margin:40,
        items:4
      },
      1280:{
        margin:22,
        items:6,
      }
    }
  });


  $('#slider').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: false,
    slideshow: false,
    sync: "#carousel",
    directionNav: false
});

$('#carousel').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: true,
    slideshow: false,
    //itemWidth: 143,
    //itemMargin: 5,
    //itemWidth: getGridSize(),
    asNavFor: '#slider',
    minItems: 2,
    maxItems: 4,
    directionNav: true,
    itemMargin: 10,
    prevText: "",
    nextText: "",
    itemWidth: 80
});
});


// flip box start
$(document).ready(function() {
  $('.card:nth-of-type(1)').hover(function(){
  $('.card:nth-of-type(1)').toggleClass('applyflip');}.bind(this));

  $('.card:nth-of-type(2)').hover(function(){
  $('.card:nth-of-type(2)').toggleClass('applyflip');}.bind(this));

  $('.card:nth-of-type(3)').hover(function(){
  $('.card:nth-of-type(3)').toggleClass('applyflip');}.bind(this));

  });

// flip box end

// Gravity form On focus label goes to top script start


$(document).ready(function() {
  
  $('.gsm-wrap >.gform_wrapper input').focus(function() {
    $(this).parent('.ginput_container').parent('.gfield').find('.gfield_label').addClass('active');
  });

  $('.gsm-wrap >.gform_wrapper input').blur(function() {
    var curTarget = $(this).val();
    if(curTarget == ''){
      $(this).parent('.ginput_container').parent('.gfield').find('.gfield_label').removeClass('active');
    }
    else{
      $(this).parent('.ginput_container').parent('.gfield').find('.gfield_label').addClass('active');
    }
  });

  $('.gsm-wrap >.gform_wrapper .ginput_container_select select').change(function(){
    var curTarget3 = $(this).find(":selected").val();
    if(curTarget3 !== ''){
      $(this).parent('.ginput_container').parent('.gfield').find('.gfield_label').addClass('active');
    }
    else{
      $(this).parent('.ginput_container').parent('.gfield').find('.gfield_label').removeClass('active');
    }
  });

});

// Gravity form On focus label goes to top script end


//Sticky Nav
// $(function() {
//   var findEl = $('.sh-sticky-wrap').length;
//   if (findEl <= 0) {
//       // do nothing
//   } else {
//     //Set the height of the sticky container to the height of the nav
//     //var navheight = $('.site-nav-container').height();
//     // grab the initial top offset of the navigation 
//     //var sticky_navigation_offset_top = $('.sh-sticky-wrap').offset().top;
//     var sticky_navigation_offset_top = $('.sh-sticky-wrap').outerHeight();
//     // our function that decides weather the navigation bar should have "fixed" css position or not.
//     var sticky_navigation = function(){
//       var scroll_top = $(window).scrollTop(); // our current vertical position from the top
//       // if we've scrolled more than the navigation, change its position to fixed to stick to top,
//       // otherwise change it back to relative
//       if (scroll_top > sticky_navigation_offset_top) { 
//         $('.sh-sticky-wrap').addClass('stuck');
//         // $('footer').css('padding-bottom',sticky_navigation_offset_top+'px');
//         //$('.sh-sticky-inner-wrap').css('height', '187px');
//       } else if(scroll_top <= sticky_navigation_offset_top) {
//         $('.sh-sticky-wrap').removeClass('stuck'); 
//         // $('footer').css('padding-bottom','0');
//         // $('.site-header').css('height', 'auto');
//       }   
//     };
//     // run our function on load
//     sticky_navigation();
//     // and run it again every time you scroll
//     $(window).scroll(function() {
//       sticky_navigation();
//     });

//   }
// });



jQuery(document).ready(function(){
  var padding_top = jQuery(".site-header").height();
  if (jQuery(window).width() >= 960) {
      jQuery(".site-content").css('padding-top', padding_top);
      }
  else{
    jQuery(".site-content").css('padding-top', padding_top);
  }
  // console.log(padding_top);
  jQuery(window).resize(function(){
    if (jQuery(window).width() >= 960) {
      var padding_top = jQuery(".site-header").height();
      jQuery(".site-content").css('padding-top', padding_top);
      // jQuery(".site-header").height();
      var padding_top = jQuery(".site-header").height();
      // console.log(padding_top);
      jQuery(".site-content").css('padding-top', padding_top);
      }
      else{
        jQuery(".site-content").css('padding-top', '181px');
      }
    });
});


//Smooth Scroll - Detects a #hash on-page link and will smooth scroll to that position. Will not affect regular links.
$(function() {
  $('.smooth-scroll').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
});



//Slide in CTA
$(function() {
    var findEl = $('#slidebox').length;
    if (findEl <= 0) {
        // do nothing
    } else {
        var slidebox = $('#slidebox');
        if (slidebox) {
            $(window).scroll(function() {
                var distanceTop = $('#last').offset().top - $(window).height();
                if ($(window).scrollTop() > distanceTop)
                    slidebox.animate({
                        'right': '0px'
                    }, 300);
                else
                    slidebox.stop(true).animate({
                        'right': '-430px'
                    }, 100);
            });
            $('#slidebox .close').on('click', function() {
                $(this).parent().remove();
            });
        }
    }
});


// include span tags around all navigation elements
$("#hs_menu_wrapper_primary_nav ul li a").each(function( index ) {
  var navText = $( this ).html(); $( this ).html("<span>" + navText + "</span>");
});


//Styles
// $(document).ready(function() {
//  $('.site-content *').removeAttr("style");
// });

$('.main-content').addClass('more height');

var wWidth = $(window).width();
if(wWidth <= 639 ){
  $( ".main-content" ).after( "<div class='link'><a id='readmore' href='javascript:changeheight()'>Show More</a></div>" );
}

$(window).resize(function() {
  var wWidth = $(window).width();
  if (wWidth < 640) {
    var addedDiv = $(".link");
    var length1= addedDiv.length;
    if (addedDiv.length == 0) {
      $(".link").remove();
      $( ".main-content" ).after( "<div class='link'><a id='readmore' href='javascript:changeheight()'>Show More</a></div>" );
    }
  }
  else if (wWidth > 639){
    $(".link").remove();
  }
   $(function() {
       var curHeight = $('.more').height();
       if (curHeight == 250)
           $('#readmore').show();
       else
           $('#readmore').hide();
   });
});
$(function() {
   var curHeight = $('.more').height();
   if (curHeight == 250)
       $('#readmore').show();
   else
       $('#readmore').hide();
});
$(window).on('resize', function() {
   $(function() {
       var curHeight = $('.more').height();
       if (curHeight == 250)
           $('#readmore').show();
       else
           $('#readmore').hide();
   });
});

function changeheight() {
   var readmore = $('#readmore');
   if (readmore.text() == 'Show More') {
       readmore.text("Show Less");
   } else {
       readmore.text("Show More");
   }

   $('.height').toggleClass("heightAuto");
};



// Animation Text Script Start
$(document).ready(function () {

  (function () {
      //set animation timing
      var animationDelay = 3500,
      //loading bar effect
          barAnimationDelay = 3800,
          barWaiting = barAnimationDelay - 3000, //3000 is the duration of the transition on the loading bar - set in the scss/css file
      //letters effect
          lettersDelay = 50,
      //type effect
          typeLettersDelay = 150,
          selectionDuration = 500,
          typeAnimationDelay = selectionDuration + 800,
      //clip effect
          revealDuration = 600,
          revealAnimationDelay = 2500;

      initHeadline();


      function initHeadline() {
          //insert <i> element for each letter of a changing word
          singleLetters($('.cd-headline.letters').find('b'));
          //initialise headline animation
          animateHeadline($('.cd-headline'));
          //set basic width on load
          $('.cd-words-wrapper').css('width', '200px');
      }

      function singleLetters($words) {
          $words.each(function(){
              var word = $(this),
                  letters = word.text().split(''),
                  selected = word.hasClass('is-visible');
              for (var i in letters) {
                  if(word.parents('.rotate-2').length > 0) letters[i] = '<em>' + letters[i] + '</em>';
                  letters[i] = (selected) ? '<i class="in">' + letters[i] + '</i>': '<i>' + letters[i] + '</i>';
              }
              var newLetters = letters.join('');
              word.html(newLetters).css('opacity', 1);
          });
      }

      function animateHeadline($headlines) {
          var duration = animationDelay;
          $headlines.each(function(){
              var headline = $(this);

              if(headline.hasClass('loading-bar')) {
                  duration = barAnimationDelay;
                  setTimeout(function(){ headline.find('.cd-words-wrapper').addClass('is-loading') }, barWaiting);
              } else if (headline.hasClass('clip')){
                  var spanWrapper = headline.find('.cd-words-wrapper'),
                      newWidth = spanWrapper.width() + 10
                  spanWrapper.css('width', newWidth);
              } else if (!headline.hasClass('type') ) {
                  //assign to .cd-words-wrapper the width of its longest word
                  var words = headline.find('.cd-words-wrapper b'),
                      width = 0;
                  words.each(function(){
                      var wordWidth = $(this).width();
                      if (wordWidth > width) width = wordWidth;
                  });
                  headline.find('.cd-words-wrapper').css('width', width);
              };

              //trigger animation
              setTimeout(function(){ hideWord( headline.find('.is-visible').eq(0) ) }, duration);
          });
      }

      function hideWord($word) {
          var nextWord = takeNext($word);

          if($word.parents('.cd-headline').hasClass('type')) {
              var parentSpan = $word.parent('.cd-words-wrapper');
              parentSpan.addClass('selected').removeClass('waiting');
              setTimeout(function(){
                  parentSpan.removeClass('selected');
                  $word.removeClass('is-visible').addClass('is-hidden').children('i').removeClass('in').addClass('out');
              }, selectionDuration);
              setTimeout(function(){ showWord(nextWord, typeLettersDelay) }, typeAnimationDelay);

          } else if($word.parents('.cd-headline').hasClass('letters')) {
              var bool = ($word.children('i').length >= nextWord.children('i').length) ? true : false;
              hideLetter($word.find('i').eq(0), $word, bool, lettersDelay);
              showLetter(nextWord.find('i').eq(0), nextWord, bool, lettersDelay);

          }  else if($word.parents('.cd-headline').hasClass('clip')) {
              $word.parents('.cd-words-wrapper').animate({ width : '2px' }, revealDuration, function(){
                  switchWord($word, nextWord);
                  showWord(nextWord);
              });

          } else if ($word.parents('.cd-headline').hasClass('loading-bar')){
              $word.parents('.cd-words-wrapper').removeClass('is-loading');
              switchWord($word, nextWord);
              setTimeout(function(){ hideWord(nextWord) }, barAnimationDelay);
              setTimeout(function(){ $word.parents('.cd-words-wrapper').addClass('is-loading') }, barWaiting);

          } else {
              switchWord($word, nextWord);
              setTimeout(function(){ hideWord(nextWord) }, animationDelay);
          }
      }

      function showWord($word, $duration) {
          if($word.parents('.cd-headline').hasClass('type')) {
              showLetter($word.find('i').eq(0), $word, false, $duration);
              $word.addClass('is-visible').removeClass('is-hidden');

          }  else if($word.parents('.cd-headline').hasClass('clip')) {
              $word.parents('.cd-words-wrapper').animate({ 'width' : $word.width() + 10 }, revealDuration, function(){
                  setTimeout(function(){ hideWord($word) }, revealAnimationDelay);
              });
          }
      }

      function hideLetter($letter, $word, $bool, $duration) {
          $letter.removeClass('in').addClass('out');

          if(!$letter.is(':last-child')) {
              setTimeout(function(){ hideLetter($letter.next(), $word, $bool, $duration); }, $duration);
          } else if($bool) {
              setTimeout(function(){ hideWord(takeNext($word)) }, animationDelay);
          }

          if($letter.is(':last-child') && $('html').hasClass('no-csstransitions')) {
              var nextWord = takeNext($word);
              switchWord($word, nextWord);
          }
      }

      function showLetter($letter, $word, $bool, $duration) {
          $letter.addClass('in').removeClass('out');

          if(!$letter.is(':last-child')) {
              setTimeout(function(){ showLetter($letter.next(), $word, $bool, $duration); }, $duration);
          } else {
              if($word.parents('.cd-headline').hasClass('type')) { setTimeout(function(){ $word.parents('.cd-words-wrapper').addClass('waiting'); }, 200);}
              if(!$bool) { setTimeout(function(){ hideWord($word) }, animationDelay) }
          }
      }

      function takeNext($word) {
          return (!$word.is(':last-child')) ? $word.next() : $word.parent().children().eq(0);
      }

      function takePrev($word) {
          return (!$word.is(':first-child')) ? $word.prev() : $word.parent().children().last();
      }

      function switchWord($oldWord, $newWord) {
          $oldWord.removeClass('is-visible').addClass('is-hidden');
          $newWord.removeClass('is-hidden').addClass('is-visible');
      }

  })();
  
});

// Animation Text Script End


// $(document).ready(function () {
//   var wWidth = $(window).width();
//   if(wWidth < 960 ){
//     $($('.rfq-btn')).insertAfter(".site-nav");
//   }
//   else {
//     $($('.rfq-btn')).insertBefore(".sh-ico-menu");
//   }
// });
// $(window).on('resize', function() {
//   var wWidth = $(window).width();
//   if(wWidth < 960 ){
//     $($('.rfq-btn')).insertAfter(".site-nav");
//   }
//   else {
//     $($('.rfq-btn')).insertBefore(".sh-ico-menu");
//   }
// }); 